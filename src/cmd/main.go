package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/pavr1/edu-grades/src/logic/external/config"
	"gitlab.com/pavr1/edu-grades/src/logic/external/http"
)

const (
	version string = "0.0.0"
)

func main() {
	app := createApp()
	err := app.Run(os.Args)

	if err != nil {
		log.Fatalf("Server failed: %v", err)
	}
}

func createApp() *cli.App {
	config := config.New()
	// tradingSettingsAPIConfig.BasicAuth.AuthHeader = tradingSettingsAuthHeader
	//log.Infof("Trading Settings API Version: %s", version)

	// var logLevel string
	// var shutdownSeconds int64

	app := cli.NewApp()
	app.Usage = "Run edu-grades server"
	//app.Version = tradingSettingsAPIConfig.Version
	app.Description = "Trading Settings provides a way to persist settings."

	config.Version = version

	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "MONGO_URL",
			EnvVars:     []string{"MONGO_URL"},
			Usage:       "Default mongo url.",
			Value:       "mongodb://mongo:27017/edu-grades",
			Destination: &config.MongoConfig.MongoURL,
		},
		&cli.StringFlag{
			Name:        "LOG_LEVEL",
			EnvVars:     []string{"LOG_LEVEL"},
			Value:       "info",
			Usage:       "log level",
			Destination: &config.LogLevel,
		},
		&cli.IntFlag{
			Name:        "MONGO_CONNECTION_TIMEOUT",
			EnvVars:     []string{"MONGO_CONNECTION_TIMEOUT"},
			Value:       5,
			Usage:       "Mongo connection timeout",
			Destination: &config.MongoConfig.ConnectionTimeout,
		},
		&cli.IntFlag{
			Name:        "MONGO_SELECTION_TIMEOUT",
			EnvVars:     []string{"MONGO_SELECTION_TIMEOUT"},
			Value:       5,
			Usage:       "Mongo selection timeout",
			Destination: &config.MongoConfig.SelectionTimeout,
		},
		&cli.IntFlag{
			Name:        "MONGO_CONNECTION_POOLSIZE",
			EnvVars:     []string{"MONGO_CONNECTION_POOLSIZE"},
			Value:       5,
			Usage:       "Mongo connection poolsize",
			Destination: &config.MongoConfig.ConnectionPoolSize,
		},
		&cli.IntFlag{
			Name:        "HTTP_PORT",
			EnvVars:     []string{"HTTP_PORT"},
			Value:       8080,
			Usage:       "http port to listen the server from",
			Destination: &config.HttpConfig.Port,
		},
		&cli.IntFlag{
			Name:        "TOKEN_EXPIRATION_TIME",
			EnvVars:     []string{"TOKEN_EXPIRATION_TIME"},
			Value:       24,
			Usage:       "token expiration time in hours",
			Destination: &config.JWTConfig.ExpirationTime,
		},
	}

	app.Before = func(ctx *cli.Context) error {
		initializeObservability(config.LogLevel)

		// err := InitTracer(tradingSettingsAPIConfig)
		// if err != nil {
		// 	log.Println("failed to init otel tracer: ", err)
		// 	return err
		// }

		// tracer = traceProvider.Tracer(name)

		// initializeObservability(tradingSettingsAPIConfig, logLevel)
		// tradingSettingsAPIConfig.ShutdownGracePeriod = time.Duration(shutdownSeconds) * time.Second
		return nil
	}

	app.Action = func(c *cli.Context) error {
		return serve(config)
	}

	app.After = func(ctx *cli.Context) error {
		// if err := traceProvider.Shutdown(ctx.Context); err != nil {
		// 	log.Printf("Error shutting down tracer provider: %v", err)
		// }
		return nil
	}

	return app
}

func serve(c *config.Config) error {
	log.Infof(" *** Creating Server...")
	s, err := createServer(c)

	if err != nil {
		return err
	}

	log.WithField("Port", c.HttpConfig.Port).Infof(" *** Server listening")
	s.Listen()

	return nil
}

func createServer(config *config.Config) (*http.HttpServer, error) {
	defer func() {
		if r := recover(); r != nil {
			log.Error("Recovered from panic.")
		}
	}()

	server, err := http.New(config)
	if err != nil {
		return nil, err
	}

	return server, nil
}

func initializeObservability(level string) {
	log.SetOutput(os.Stdout)
	log.SetReportCaller(true)
	log.SetFormatter(&log.JSONFormatter{})

	logLevel := log.InfoLevel
	switch level {
	case "debug":
		logLevel = log.DebugLevel
	case "warn":
		logLevel = log.WarnLevel
	case "error":
		logLevel = log.ErrorLevel
	}
	log.SetLevel(logLevel)
}
