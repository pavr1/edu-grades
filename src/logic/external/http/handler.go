package http

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pavr1/edu-grades/src/logic/external/config"
	"gitlab.com/pavr1/edu-grades/src/logic/external/http/base"
	"gitlab.com/pavr1/edu-grades/src/logic/external/http/handlers"
	"gitlab.com/pavr1/edu-grades/src/logic/internal/db/repo"
)

type HttpServer struct {
	base.HtttpParent
	port   int
	router *mux.Router
	repo   *repo.MongoRepo
}

func New(c *config.Config) (*HttpServer, error) {
	server := &HttpServer{
		port: c.HttpConfig.Port,
	}

	repo, err := repo.New(c)
	if err != nil {
		return nil, err
	}

	server.repo = repo

	router := mux.NewRouter()

	studentHandler := handlers.NewStudent(server.WriteResponse, server.getRepo)
	schoolHandler := handlers.NewSchool(server.WriteResponse, server.getRepo)
	authenticationHandler := handlers.NewAuthentication(time.Duration(c.JWTConfig.ExpirationTime)*time.Hour, server.WriteResponse, server.getRepo)
	sectionHandler := handlers.NewSection(server.WriteResponse, server.getRepo, schoolHandler, authenticationHandler)

	router.HandleFunc("/", server.homeHandler).Methods("GET")
	router.HandleFunc("/students", studentHandler.Get).Methods("GET")
	router.HandleFunc("/students/create", studentHandler.Create).Methods("POST")
	router.HandleFunc("/schools", schoolHandler.Get).Methods("GET")
	router.HandleFunc("/schools/create", schoolHandler.Create).Methods("POST")
	router.HandleFunc("/sections", sectionHandler.Get).Methods("GET")
	router.HandleFunc("/sections/create", sectionHandler.Create).Methods("POST")
	router.HandleFunc("/authentication", authenticationHandler.Get).Methods("GET")
	router.HandleFunc("/authentication/create", authenticationHandler.Create).Methods("POST")

	server.router = router

	return server, nil
}

func (s *HttpServer) Listen() {
	err := http.ListenAndServe(fmt.Sprintf(":%d", s.port), s.router)
	if err != nil {
		log.WithError(err).WithField("Port", s.port).Fatal("Failed to start server")
		panic(err)
	}
}

func (s *HttpServer) homeHandler(w http.ResponseWriter, r *http.Request) {
	s.WriteResponse(w, http.StatusOK, []byte("Welcome to the home page!"))
}

func (s *HttpServer) getRepo() *repo.MongoRepo {
	return s.repo
}
