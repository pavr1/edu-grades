package handlers

import (
	"encoding/json"
	"io"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/pavr1/edu-grades/src/logic/external/http/handlers/base"
	"gitlab.com/pavr1/edu-grades/src/logic/external/models"
	"gitlab.com/pavr1/edu-grades/src/logic/internal/db/repo"
	"go.mongodb.org/mongo-driver/bson"
)

type Authentication struct {
	base.JWTAuthenticator
	tokenExpirationTime time.Duration
	writeResponseFunc   func(w http.ResponseWriter, status int, data []byte)
	getRepoFunc         func() *repo.MongoRepo
}

func NewAuthentication(tokenExpirationTime time.Duration, writeResponseFunc func(w http.ResponseWriter, status int, data []byte), getRepoFunc func() *repo.MongoRepo) *Authentication {
	return &Authentication{
		tokenExpirationTime: tokenExpirationTime,
		writeResponseFunc:   writeResponseFunc,
		getRepoFunc:         getRepoFunc,
	}
}

func (a *Authentication) Get(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error("Error reading request body")

		a.writeResponseFunc(w, http.StatusInternalServerError, []byte("Error reading request body"))
		return
	}

	var entity models.Authentication
	err = json.Unmarshal(body, &entity)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON")

		a.writeResponseFunc(w, http.StatusBadRequest, []byte("Error decoding body to JSON"))
		return
	}

	if entity.User == "" || entity.Password == "" {
		log.Error("Invalid User or Password")

		a.writeResponseFunc(w, http.StatusBadRequest, []byte("Invalid User or Password"))
		return
	}

	entities, err := a.GetByUserAndPwd(entity.User, entity.Password)
	if err != nil {
		a.writeResponseFunc(w, http.StatusInternalServerError, []byte(err.Error()))
		return
	}

	status := 0
	var response *models.AuthenticationResponse
	if len(entities) == 0 {
		status = http.StatusUnauthorized

		a.writeResponseFunc(w, http.StatusUnauthorized, []byte("Invalid User or Password"))
		return
	}

	status = http.StatusOK

	auth := entities[0].(*models.Authentication)

	log.Info("User authenticated, generating JWT token")
	response = &models.AuthenticationResponse{
		Token: a.JWTAuthenticator.GenerateToken(entity.User, a.tokenExpirationTime),
		ID:    auth.AuthenticationID,
		User:  auth.User,
	}

	jsonData, err := json.Marshal(response)
	if err != nil {
		log.WithError(err).Error("Error marshalling authentications")
		a.writeResponseFunc(w, http.StatusInternalServerError, []byte(err.Error()))
		return
	}

	log.WithField("Count", len(entities)).Info("authentication retrieved successfully")
	a.writeResponseFunc(w, status, []byte(jsonData))
}

func (a *Authentication) Create(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error("Error reading request body")

		a.writeResponseFunc(w, http.StatusBadRequest, []byte("Error reading request body"))
		return
	}

	var entity models.Authentication
	err = json.Unmarshal(body, &entity)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON")

		a.writeResponseFunc(w, http.StatusBadRequest, []byte("Error decoding JSON"))
		return
	}

	entity.AuthenticationID = a.getRepoFunc().GenerateID()

	result, cerr := a.getRepoFunc().Create(&entity)
	if cerr != nil {
		a.writeResponseFunc(w, http.StatusInternalServerError, []byte(cerr.Error()))
		return
	}

	log.WithField("Result", result).Info("Authentication created successfully")
	a.writeResponseFunc(w, http.StatusOK, []byte("Authentication created successfully"))
}

func (s *Authentication) GetByUserAndPwd(user, password string) ([]interface{}, error) {
	filter := bson.D{
		bson.E{Key: "user", Value: user},
		bson.E{Key: "password", Value: password},
	}

	entities, err := s.getRepoFunc().Get(&models.Authentication{}, filter)
	if err != nil {
		return nil, err
	}
	return entities, nil
}

func (s *Authentication) GetByTeacherID(teacherID string) ([]interface{}, error) {
	filter := bson.D{
		bson.E{Key: "authenticationid", Value: teacherID},
	}

	entities, err := s.getRepoFunc().Get(&models.Authentication{}, filter)
	if err != nil {
		return nil, err
	}
	return entities, nil
}
