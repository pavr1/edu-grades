package handlers

import (
	"encoding/json"
	"io"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/pavr1/edu-grades/src/logic/external/http/handlers/base"
	"gitlab.com/pavr1/edu-grades/src/logic/external/models"
	"gitlab.com/pavr1/edu-grades/src/logic/internal/db/repo"
	"go.mongodb.org/mongo-driver/bson"
)

type Student struct {
	base.JWTAuthenticator
	writeResponseFunc func(w http.ResponseWriter, status int, data []byte)
	getRepoFunc       func() *repo.MongoRepo
}

func NewStudent(writeResponseFunc func(w http.ResponseWriter, status int, data []byte), getRepoFunc func() *repo.MongoRepo) *Student {
	return &Student{
		writeResponseFunc: writeResponseFunc,
		getRepoFunc:       getRepoFunc,
	}
}

func (s *Student) Get(w http.ResponseWriter, r *http.Request) {
	valid := s.JWTAuthenticator.Authenticate(w, r)
	if !valid {
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Invalid JWT token"))
		return
	}

	entity := models.Student{}
	entitites, cerr := s.getRepoFunc().Get(&entity, bson.D{})
	if cerr != nil {
		s.writeResponseFunc(w, http.StatusInternalServerError, []byte(cerr.Error()))
		return
	}

	jsonData, err := json.Marshal(entitites)
	if err != nil {
		log.WithError(err).Error("Error marshalling students")
		s.writeResponseFunc(w, http.StatusInternalServerError, []byte(err.Error()))
		return
	}

	log.WithField("Count", len(entitites)).Info("students retrieved successfully")
	s.writeResponseFunc(w, http.StatusOK, []byte(jsonData))
}

func (s *Student) Create(w http.ResponseWriter, r *http.Request) {
	valid := s.JWTAuthenticator.Authenticate(w, r)
	if !valid {
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Invalid JWT token"))
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error("Error reading request body")

		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Error reading request body"))
		return
	}

	var entity models.Student
	err = json.Unmarshal(body, &entity)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON")

		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Error decoding JSON"))
		return
	}

	entity.StudentID = s.getRepoFunc().GenerateID()

	result, cerr := s.getRepoFunc().Create(&entity)
	if cerr != nil {
		s.writeResponseFunc(w, http.StatusInternalServerError, []byte(cerr.Error()))
		return
	}

	log.WithField("Result", result).Info("Student created successfully")
	s.writeResponseFunc(w, http.StatusOK, []byte("Student created successfully"))
}
