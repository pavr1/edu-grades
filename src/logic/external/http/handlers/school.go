package handlers

import (
	"encoding/json"
	"io"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/pavr1/edu-grades/src/logic/external/http/handlers/base"
	"gitlab.com/pavr1/edu-grades/src/logic/external/models"
	"gitlab.com/pavr1/edu-grades/src/logic/internal/db/repo"
	"go.mongodb.org/mongo-driver/bson"
)

type School struct {
	base.JWTAuthenticator
	writeResponseFunc func(w http.ResponseWriter, status int, data []byte)
	getRepoFunc       func() *repo.MongoRepo
}

func NewSchool(writeResponseFunc func(w http.ResponseWriter, status int, data []byte), getRepoFunc func() *repo.MongoRepo) *School {
	return &School{
		writeResponseFunc: writeResponseFunc,
		getRepoFunc:       getRepoFunc,
	}
}

func (s *School) Get(w http.ResponseWriter, r *http.Request) {
	valid := s.JWTAuthenticator.Authenticate(w, r)
	if !valid {
		log.Error("Invalid JWT token")

		s.writeResponseFunc(w, http.StatusInternalServerError, []byte("Invalid JWT token"))
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error("Error reading request body")

		s.writeResponseFunc(w, http.StatusInternalServerError, []byte("Error reading request body"))
		return
	}

	entity := models.School{}
	err = json.Unmarshal(body, &entity)
	if err != nil {
		log.WithField("Body", string(body)).WithError(err).Error("Error decoding JSON")

		s.writeResponseFunc(w, http.StatusInternalServerError, []byte("Error decoding body to JSON"))
		return
	}

	if entity.TeacherID == "" {
		log.Error("TeacherID is required")
		s.writeResponseFunc(w, http.StatusInternalServerError, []byte("TeacherID is required"))
		return
	}

	entities, err := s.GetByTeacherID(entity.TeacherID)
	if err != nil {
		s.writeResponseFunc(w, http.StatusInternalServerError, []byte(err.Error()))
		return
	}

	jsonData, err := json.Marshal(entities)
	if err != nil {
		log.WithError(err).Error("Error marshalling schools")
		s.writeResponseFunc(w, http.StatusInternalServerError, []byte(err.Error()))
		return
	}

	log.WithField("Count", len(entities)).Info("scools retrieved successfully")
	s.writeResponseFunc(w, http.StatusOK, []byte(jsonData))
}

func (s *School) Create(w http.ResponseWriter, r *http.Request) {
	valid := s.JWTAuthenticator.Authenticate(w, r)
	if !valid {
		log.Error("Invalid JWT token")

		s.writeResponseFunc(w, http.StatusUnauthorized, []byte("Invalid JWT token"))
		return
	}
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error("Error reading request body")

		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Error reading request body"))
		return
	}

	var entity models.School
	err = json.Unmarshal(body, &entity)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON")

		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Error decoding JSON"))
		return
	}

	if entity.TeacherID == "" {
		log.Error("TeacherID is required")

		s.writeResponseFunc(w, http.StatusBadRequest, []byte("TeacherID is required"))
		return
	}

	//todo: validate teacherID exists

	if entity.Name == "" {
		log.Error("Name is required")

		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Name is required"))
		return
	}

	entity.SchoolID = s.getRepoFunc().GenerateID()

	result, cerr := s.getRepoFunc().Create(&entity)
	if cerr != nil {
		s.writeResponseFunc(w, http.StatusInternalServerError, []byte(cerr.Error()))
		return
	}

	log.WithField("Result", result).Info("School created successfully")
	s.writeResponseFunc(w, http.StatusOK, []byte("School created successfully"))
}

func (s *School) GetByTeacherID(teacherID string) ([]interface{}, error) {
	filter := bson.D{
		bson.E{Key: "teacherid", Value: teacherID},
	}

	entities, err := s.getRepoFunc().Get(&models.School{}, filter)
	if err != nil {
		return nil, err
	}
	return entities, nil
}

func (s *School) GetBySchoolID(schoolID string) ([]interface{}, error) {
	filter := bson.D{
		bson.E{Key: "schoolid", Value: schoolID},
	}

	entities, err := s.getRepoFunc().Get(&models.School{}, filter)
	if err != nil {
		return nil, err
	}
	return entities, nil
}
