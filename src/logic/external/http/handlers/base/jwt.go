package base

import (
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
)

type JWTAuthenticator struct {
}

func (j *JWTAuthenticator) GenerateToken(user string, expirationTime time.Duration) string {
	// Generate a JWT token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set the claims for the token
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = user
	claims["exp"] = time.Now().Add(expirationTime).Unix()

	// todo - Replace "your-secret-key" with your own secret key
	tokenString, err := token.SignedString([]byte("your-secret-key"))
	if err != nil {
		log.WithError(err).Error("Error generating JWT token")
		return ""
	}

	return tokenString
}

func (j *JWTAuthenticator) Authenticate(w http.ResponseWriter, r *http.Request) bool {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		log.Error("Missing Authorization header")
		return false
	}

	// Split the Authorization header into the bearer token and the actual token value
	authHeaderParts := strings.Split(authHeader, " ")
	if len(authHeaderParts) != 2 || authHeaderParts[0] != "Bearer" {
		log.Error("Invalid Authorization header")
		return false
	}

	// Get the token value
	tokenValue := authHeaderParts[1]

	// Parse the token
	token, err := jwt.Parse(tokenValue, func(token *jwt.Token) (interface{}, error) {
		// Provide the key or public key to verify the token's signature
		// You can customize this based on your implementation
		return []byte("your-secret-key"), nil
	})
	if err != nil {
		log.WithError(err).Error("Error parsing JWT token")
		return false
	}

	// Check if the token is valid
	if !token.Valid {
		log.WithField("Valid", token.Valid).Error("Invalid JWT token")
		return false
	}

	return true
}
