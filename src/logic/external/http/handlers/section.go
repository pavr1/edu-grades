package handlers

import (
	"encoding/json"
	"io"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/pavr1/edu-grades/src/logic/external/http/handlers/base"
	"gitlab.com/pavr1/edu-grades/src/logic/external/models"
	"gitlab.com/pavr1/edu-grades/src/logic/internal/db/repo"
	"go.mongodb.org/mongo-driver/bson"
)

type Section struct {
	base.JWTAuthenticator
	school            *School
	authentication    *Authentication
	writeResponseFunc func(w http.ResponseWriter, status int, data []byte)
	getRepoFunc       func() *repo.MongoRepo
}

func NewSection(writeResponseFunc func(w http.ResponseWriter, status int, data []byte), getRepoFunc func() *repo.MongoRepo, school *School, authentication *Authentication) *Section {
	return &Section{
		school:            school,
		authentication:    authentication,
		writeResponseFunc: writeResponseFunc,
		getRepoFunc:       getRepoFunc,
	}
}

func (s *Section) Get(w http.ResponseWriter, r *http.Request) {
	valid := s.JWTAuthenticator.Authenticate(w, r)
	if !valid {
		s.writeResponseFunc(w, http.StatusUnauthorized, []byte("Invalid JWT token"))
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error("Error reading request body")

		s.writeResponseFunc(w, http.StatusInternalServerError, []byte("Error reading request body"))
		return
	}

	entity := models.Section{}
	err = json.Unmarshal(body, &entity)
	if err != nil {
		log.WithField("Body", string(body)).WithError(err).Error("Error decoding JSON")

		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Error decoding body to JSON"))
		return
	}

	if entity.TeacherID == "" {
		log.Error("TeacherID is required")

		s.writeResponseFunc(w, http.StatusBadRequest, []byte("TeacherID is required"))
		return
	}

	entities, err := s.GetByTeacherID(entity.TeacherID)
	if err != nil {
		s.writeResponseFunc(w, http.StatusInternalServerError, []byte(err.Error()))
		return
	}

	if len(entities) == 0 {
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Invalid teacher id"))
		return
	}

	jsonData, err := json.Marshal(entities)
	if err != nil {
		log.WithError(err).Error("Error marshalling sections")
		s.writeResponseFunc(w, http.StatusInternalServerError, []byte(err.Error()))
		return
	}

	log.WithField("Count", len(entities)).Info("sections retrieved successfully")
	s.writeResponseFunc(w, http.StatusOK, []byte(jsonData))
}

func (s *Section) Create(w http.ResponseWriter, r *http.Request) {
	valid := s.JWTAuthenticator.Authenticate(w, r)
	if !valid {
		s.writeResponseFunc(w, http.StatusUnauthorized, []byte("Invalid JWT token"))
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Error reading request body"))
		return
	}

	var entity models.Section
	err = json.Unmarshal(body, &entity)
	if err != nil {
		log.WithError(err).Error("Error decoding JSON")
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Error unmarshalling body"))
		return
	}

	//todo add gradeID validation

	if entity.TeacherID == "" {
		log.Error("TeacherID is required")
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Teacher id required"))
		return
	}

	if entity.SchoolID == "" {
		log.Error("SchoolID is required")
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("School id required"))
		return
	}

	if entity.Grade == "" {
		log.Error("Grade is required")
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Grade id required"))
		return
	}

	if entity.Year == "" {
		log.Error("Year is required")
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Year required"))
		return
	}

	if entity.Name == "" {
		log.Error("Name is required")
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Name required"))
		return
	}

	entities, err := s.school.GetBySchoolID(entity.SchoolID)
	if err != nil {
		log.WithField("SchoolID", entity.SchoolID).WithError(err).Error("Error retrieving schools")
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Error retrieving schools"))
		return
	}

	if len(entities) == 0 {
		log.WithField("SchoolID", entity.SchoolID).Error("Invalid SchoolID")
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Invalid SchoolID"))
		return
	}

	entities, err = s.authentication.GetByTeacherID(entity.TeacherID)
	if err != nil {
		log.WithField("TeacherID", entity.TeacherID).WithError(err).Error("Error retrieving teacher")
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Error retrieving teacher"))
		return
	}

	if len(entities) == 0 {
		log.WithField("TeacherID", entity.SchoolID).Error("Invalid TeacherID")
		s.writeResponseFunc(w, http.StatusBadRequest, []byte("Invalid TeacherID"))
		return
	}

	entity.SectionID = s.getRepoFunc().GenerateID()

	result, cerr := s.getRepoFunc().Create(&entity)
	if cerr != nil {
		s.writeResponseFunc(w, http.StatusInternalServerError, []byte(cerr.Error()))
		return
	}

	log.WithField("Result", result).Info("Section created successfully")
	s.writeResponseFunc(w, http.StatusOK, []byte("Section created successfully"))
}

func (s *Section) GetByTeacherID(teacherID string) ([]interface{}, error) {
	filter := bson.D{
		bson.E{Key: "teacherid", Value: teacherID},
	}

	entities, err := s.getRepoFunc().Get(&models.Section{}, filter)
	if err != nil {
		return nil, err
	}
	return entities, nil
}
