package base

import (
	"net/http"

	log "github.com/sirupsen/logrus"
)

const (
	contentTypeHeaderKey       = "Content-Type"
	applicationJSONHeaderValue = "application/json"
)

type HtttpParent struct {
}

func (h *HtttpParent) WriteResponse(w http.ResponseWriter, statusCode int, jsonResponse []byte) {
	w.Header().Set(contentTypeHeaderKey, applicationJSONHeaderValue)
	w.WriteHeader(statusCode)
	_, err := w.Write(jsonResponse)
	if err != nil {
		log.WithError(err).Error("Error writing response")
		return
	}
}
