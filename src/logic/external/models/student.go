package models

import "gitlab.com/pavr1/edu-grades/src/logic/external/models/base"

type Student struct {
	base.Entity
	StudentID         string `json:"id"`
	Name              string `json:"Name"`
	LastName          string `json:"LastName"`
	SecondLastName    string `json:"SecondLastName"`
	SpecialAdaptation string `json:"SpecialAdaptation"` //adecuacion especial
	Responsible       string `json:"Responsible"`
	Telephone         string `json:"Telephone"`
}
