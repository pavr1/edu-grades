package models

import "gitlab.com/pavr1/edu-grades/src/logic/external/models/base"

type School struct {
	base.Entity
	SchoolID  string `json:"id"`
	TeacherID string `json:"teacherId"`
	Name      string `json:"Name"`
}
