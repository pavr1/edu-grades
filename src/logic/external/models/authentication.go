package models

import "gitlab.com/pavr1/edu-grades/src/logic/external/models/base"

type Authentication struct {
	base.Entity
	AuthenticationID string `json:"authenticationid"`
	User             string `json:"User"`
	Password         string `json:"Password"`
	Name             string `json:"Name"`
}

type AuthenticationResponse struct {
	ID    string `json:"authenticationid"`
	User  string `json:"User"`
	Token string `json:"Token"`
}
