package models

import "fmt"

type Error struct {
	ErrorCode int
	Err       error
}

func (e *Error) Error() string {
	return fmt.Sprintf("Error Code: %d, Error: %s\n", e.ErrorCode, e.Err.Error())
}
