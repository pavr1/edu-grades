package models

import "gitlab.com/pavr1/edu-grades/src/logic/external/models/base"

type Section struct {
	base.Entity
	SectionID string `json:"id"`
	Grade     string `json:"grade"`
	TeacherID string `json:"teacherid"`
	SchoolID  string `json:"schoolid"`
	Year      string `json:"year"`
	Name      string `json:"name"`
}
