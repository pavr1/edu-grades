package config

type Config struct {
	Version     string
	LogLevel    string
	MongoConfig *MongoConfig
	HttpConfig  *HttpConfig
	JWTConfig   *JWTConfig
}

type MongoConfig struct {
	MongoURL           string
	ConnectionTimeout  int
	SelectionTimeout   int
	ConnectionPoolSize int
}

type HttpConfig struct {
	Port int
}

type JWTConfig struct {
	ExpirationTime int
}

func New() *Config {
	return &Config{
		MongoConfig: &MongoConfig{},
		HttpConfig:  &HttpConfig{},
		JWTConfig:   &JWTConfig{},
	}
}
