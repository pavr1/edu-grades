package repo

import (
	"context"
	"errors"
	"net/http"
	"time"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pavr1/edu-grades/src/logic/external/config"
	"gitlab.com/pavr1/edu-grades/src/logic/external/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.mongodb.org/mongo-driver/x/mongo/driver/connstring"
)

const (
	databaseName = "edu-grades"
)

type MongoRepo struct {
	timeout  time.Duration
	client   *mongo.Client
	database *mongo.Database
}

func New(c *config.Config) (*MongoRepo, error) {
	log.WithField("Url", c.MongoConfig.MongoURL).Infof(" *** Connecting to MongoDB...")

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(c.MongoConfig.ConnectionTimeout)*time.Second)
	defer cancel()

	opts := options.Client()
	opts = opts.ApplyURI(c.MongoConfig.MongoURL)
	opts = opts.SetReadConcern(readconcern.Local())
	opts = opts.SetReadPreference(readpref.Primary())
	opts = opts.SetConnectTimeout(time.Duration(c.MongoConfig.ConnectionTimeout) * time.Second)
	opts = opts.SetServerSelectionTimeout(time.Duration(c.MongoConfig.SelectionTimeout) * time.Second)
	opts = opts.SetMaxPoolSize(uint64(c.MongoConfig.ConnectionPoolSize))

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		log.WithError(err).Error("\tFailed to connect to MongoDB")

		return nil, err
	}

	connectionString, err := connstring.Parse(c.MongoConfig.MongoURL)
	if err != nil {
		log.WithError(err).Error("\tFailed to parse MongoDB connection string")

		return nil, err
	}

	ctxPing, cancelPing := context.WithTimeout(context.Background(), time.Duration(c.MongoConfig.SelectionTimeout)*time.Second)
	defer cancelPing()

	err = client.Ping(ctxPing, readpref.Primary())
	if err != nil {
		log.WithError(err).Error("\tFailed to ping MongoDB")

		return nil, err
	}

	database := client.Database(connectionString.Database)
	log.Info("\tConnected to MongoDB!")

	return &MongoRepo{
		client:   client,
		database: database,
		timeout:  time.Duration(c.MongoConfig.ConnectionTimeout) * time.Second,
	}, nil
}

func (r *MongoRepo) Close() {
	log.Info("Closing MongoDB connection...")
	err := r.client.Disconnect(context.Background())
	if err != nil {
		log.WithError(err).Error("Failed to close MongoDB connection")
	}
}

func (r *MongoRepo) Create(entity interface{}) (*mongo.InsertOneResult, *models.Error) {
	collectionName := ""

	switch v := entity.(type) {
	case *models.Student:
		collectionName = "Students"
	case *models.School:
		collectionName = "Schools"
	case *models.Authentication:
		collectionName = "Authentications"
	case *models.Section:
		collectionName = "Sections"
	default:
		err := errors.New("Entity not created, invalid entity type")
		log.WithError(err).WithField("Entity Type", v).Error("Error creating entity")

		return nil, &models.Error{
			ErrorCode: http.StatusBadRequest,
			Err:       err,
		}
	}

	insertResult, err := r.database.Collection(collectionName).InsertOne(context.Background(), entity)
	if err != nil {
		log.WithError(err).WithField("CollectionName", collectionName).Error("Error creating student")
		return nil, &models.Error{
			ErrorCode: http.StatusInternalServerError,
			Err:       err,
		}
	}

	return insertResult, nil
}

func (r *MongoRepo) Get(entity interface{}, filters bson.D) ([]interface{}, *models.Error) {
	collectionName := ""

	switch v := entity.(type) {
	case *models.Student:
		collectionName = "Students"
	case *models.School:
		collectionName = "Schools"
	case *models.Authentication:
		collectionName = "Authentications"
	case *models.Section:
		collectionName = "Sections"
	default:
		err := errors.New("Invalid entity type")
		log.WithError(err).WithField("Entity Type", v).Error("Error retrieving entity")

		return nil, &models.Error{
			ErrorCode: http.StatusBadRequest,
			Err:       err,
		}
	}

	collection := r.database.Collection(collectionName)

	// Set a timeout for the database operation
	ctx, cancel := context.WithTimeout(context.Background(), r.timeout)
	defer cancel()

	cursor, err := collection.Find(ctx, filters)
	if err != nil {
		log.WithError(err).WithField("Filter", filters).Error("Error retrieving documents")
		return nil, &models.Error{
			ErrorCode: http.StatusInternalServerError,
			Err:       err,
		}
	}
	defer cursor.Close(ctx)

	var entities []interface{}
	entities = make([]interface{}, 0)

	for cursor.Next(ctx) {
		switch v := entity.(type) {
		case *models.Student:
			var student models.Student
			err := cursor.Decode(&student)
			if err != nil {
				log.WithError(err).WithField("Entity", "Student").Error("Error decoding student")

				return nil, &models.Error{
					ErrorCode: http.StatusInternalServerError,
					Err:       err,
				}
			}
			entities = append(entities, &student)
		case *models.School:
			var school models.School
			err := cursor.Decode(&school)
			if err != nil {
				log.WithError(err).WithField("Entity", "School").Error("Error decoding student")

				return nil, &models.Error{
					ErrorCode: http.StatusInternalServerError,
					Err:       err,
				}
			}
			entities = append(entities, &school)
		case *models.Authentication:
			var authentication models.Authentication
			err := cursor.Decode(&authentication)
			if err != nil {
				log.WithError(err).WithField("Entity", "Authentication").Error("Error decoding authentication")

				return nil, &models.Error{
					ErrorCode: http.StatusInternalServerError,
					Err:       err,
				}
			}
			entities = append(entities, &authentication)
		case *models.Section:
			var section models.Section
			err := cursor.Decode(&section)
			if err != nil {
				log.WithError(err).WithField("Entity", "Section").Error("Error decoding section")

				return nil, &models.Error{
					ErrorCode: http.StatusInternalServerError,
					Err:       err,
				}
			}
			entities = append(entities, &section)
		default:
			err := errors.New("Invalid entity type")
			log.WithError(err).WithField("Entity Type", v).Error("Error retrieving entity")

			return nil, &models.Error{
				ErrorCode: http.StatusBadRequest,
				Err:       err,
			}
		}
	}

	// Check for errors during cursor iteration
	if err := cursor.Err(); err != nil {
		log.WithError(err).Error("Error iterating cursor")

		return nil, &models.Error{
			ErrorCode: http.StatusInternalServerError,
			Err:       err,
		}
	}

	return entities, nil
}

func (r *MongoRepo) GenerateID() string {
	id := uuid.New()

	return id.String()
}
