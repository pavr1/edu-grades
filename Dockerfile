FROM alpine:latest as alpine
RUN apk add --no-cache tzdata ca-certificates


FROM golang:1.19.8 as gobuild

ENV CGO_ENABLED 0
ARG GO_LDFLAGS

WORKDIR /opt

COPY . .

RUN go build -o build/edu-grades -buildvcs=false -ldflags "${GO_LDFLAGS}" ./src/cmd/

RUN groupadd -g 3000 appuser && useradd -r -u 1000 -g appuser appuser
RUN chown -R appuser:appuser build/edu-grades

FROM scratch as release
COPY --from=alpine /etc/ssl/certs /etc/ssl/certs
COPY --from=gobuild /etc/passwd /etc/passwd
COPY --from=gobuild /opt/build/edu-grades /opt/edu-grades

ENV LOG_LEVEL debug

USER appuser

WORKDIR /opt
ENTRYPOINT ["/opt/edu-grades"]

EXPOSE 8080